from Data import Data


class State(object):
    temperature: Data
    pollution: Data
    humidity: Data

    def __init__(self, temperature: Data, pollution: Data, humidity: Data):
        self.temperature = temperature
        self.pollution = pollution
        self.humidity = humidity
