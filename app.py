import threading

from common import app, state
from update import update
from communication import getCurrentValuesFromSensors

# Poniższe importy są ważne i muszą pozostać, ponieważ
# aplikacja nie będzie działała poprawnie
import temperature
import pollution
import humidity

threading.Thread(target=update, args=(state.temperature,)).start()
threading.Thread(target=update, args=(state.pollution,)).start()
threading.Thread(target=update, args=(state.humidity,)).start()
threading.Thread(target=getCurrentValuesFromSensors).start()

if __name__ == '__main__':
    app.run()
