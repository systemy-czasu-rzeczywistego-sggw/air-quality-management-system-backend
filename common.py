import json

from flask import Flask
from flask import request

from Data import Data
from State import State

isFirstRun: bool = True

app = Flask(__name__)

state = State(
    Data(15.0),
    Data(),
    Data(45.0)
)


@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    response.headers.add('Access-Control-Allow-Headers', '*')
    response.headers.add('Access-Control-Allow-Methods', '*')
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    response.headers.add("Content-Type", "application/json; charset=utf-8")
    return response


@app.route('/currentState')
def getCurrentState():
    return json.dumps(state, default=lambda x: x.__dict__)


def ifCanEditSetExpectedValue(data: Data):
    if data.enabledAutoMode:
        json_data = request.json
        if not json_data:
            return
        expected_value_as_str: str = json_data['value']
        if expected_value_as_str in (None, ''):
            return
        data.expectedValue = float(expected_value_as_str)


def setIsFirstRun(value: bool):
    isFirstRun = value
