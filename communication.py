from time import sleep
from typing import List

import serial
from pydantic import BaseModel
from serial import Serial

from Data import Data
from common import state, isFirstRun


class AirData(BaseModel):
    pollution: float
    humidity: float
    temperature: float


def getCurrentValuesFromSensors():
    data_from_sensors_as_float_list: List[float] = []
    server: Serial = serial.Serial("/dev/ttyACM0", 19200)

    while True:
        data_from_sensors_as_list: List[str] = getSeparatedDataFromSensors(server)

        if noCompleteRequiredDataFromThreeRequiredSensors(data_from_sensors_as_list):
            continue

        for item in data_from_sensors_as_list:
            data_from_sensors_as_float_list.append(float(item))

        air_data = AirData(pollution=data_from_sensors_as_float_list[0], humidity=data_from_sensors_as_float_list[1],
                           temperature=data_from_sensors_as_float_list[2])

        print(air_data.json())

        updateRealValue(state.temperature, air_data.temperature)
        updateRealValue(state.pollution, air_data.pollution)
        updateRealValue(state.humidity, air_data.humidity)

        data_from_sensors_as_float_list.clear()
        data_from_sensors_as_list.clear()

        sleep(1)


def getSeparatedDataFromSensors(server: Serial) -> List[str]:
    dataAsBytes: bytes = server.readline()
    decodedData = str(dataAsBytes[0:len(dataAsBytes)].decode("utf-8"))
    return decodedData.split('x')


def noCompleteRequiredDataFromThreeRequiredSensors(separated_data: List[str]) -> bool:
    return separated_data is None or len(separated_data) < 3 or '' in separated_data


def updateRealValue(data: Data, value: float):
    if not data.enabledAutoMode:
        data.realValueFromSensor = value
        if isFirstRun:
            data.currentValue = value
