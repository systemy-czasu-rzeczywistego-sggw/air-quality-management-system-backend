from common import app, state, ifCanEditSetExpectedValue, getCurrentState, setIsFirstRun


@app.route('/humidity/toggle', methods=['GET', 'POST', 'OPTIONS'])
def toggleHumidityEdit():
    setIsFirstRun(False)
    state.humidity.enabledAutoMode = not state.humidity.enabledAutoMode
    if state.humidity.enabledAutoMode:
        state.humidity.expectedValue = state.humidity.currentValue
    return getCurrentState()


@app.route('/humidity', methods=['GET', 'POST', 'OPTIONS'])
def setExpectedHumidity():
    ifCanEditSetExpectedValue(state.humidity)
    return getCurrentState()
