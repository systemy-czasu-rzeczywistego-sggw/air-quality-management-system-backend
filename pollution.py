from common import app, state, ifCanEditSetExpectedValue, getCurrentState, setIsFirstRun


@app.route('/pollution/toggle', methods=['GET', 'POST', 'OPTIONS'])
def togglePollutionEdit():
    setIsFirstRun(False)
    state.pollution.enabledAutoMode = not state.pollution.enabledAutoMode
    if state.pollution.enabledAutoMode:
        state.pollution.expectedValue = state.pollution.currentValue
    return getCurrentState()


@app.route('/pollution', methods=['GET', 'POST', 'OPTIONS'])
def setExpectedPollution():
    ifCanEditSetExpectedValue(state.pollution)
    return getCurrentState()
