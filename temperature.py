from common import app, state, ifCanEditSetExpectedValue, getCurrentState, setIsFirstRun


@app.route('/temperature/toggle', methods=['GET', 'POST', 'OPTIONS'])
def toggleTemperatureEdit():
    setIsFirstRun(False)
    state.temperature.enabledAutoMode = not state.temperature.enabledAutoMode
    if state.temperature.enabledAutoMode:
        state.temperature.expectedValue = state.temperature.currentValue
    return getCurrentState()


@app.route('/temperature', methods=['GET', 'POST', 'OPTIONS'])
def setExpectedTemperature():
    ifCanEditSetExpectedValue(state.temperature)
    return getCurrentState()
