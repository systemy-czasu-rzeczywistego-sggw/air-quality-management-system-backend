import time

from Data import Data


def update(data: Data):
    while True:
        ifEnabledAutomaticMode(data)
        ifDisabledAutomaticMode(data)
        time.sleep(1)


def ifEnabledAutomaticMode(data: Data):
    if data.enabledAutoMode:
        ifCurrentValueIsGreaterThanExpected(data)
        ifCurrentValueIsLessThanExpected(data)


def ifDisabledAutomaticMode(data: Data):
    if not data.enabledAutoMode:
        ifCurrentValueIsGreaterThanRealValue(data)
        ifCurrentValueIsLessThanRealValue(data)


def ifCurrentValueIsGreaterThanExpected(data: Data):
    if data.currentValue > data.expectedValue:
        data.currentValue -= 1


def ifCurrentValueIsLessThanExpected(data: Data):
    if data.currentValue < data.expectedValue:
        data.currentValue += 1


def ifCurrentValueIsGreaterThanRealValue(data: Data):
    if data.currentValue > data.realValueFromSensor:
        data.currentValue -= 1


def ifCurrentValueIsLessThanRealValue(data: Data):
    if data.currentValue < data.realValueFromSensor:
        data.currentValue += 1
